package com.example.juancarlos.appumg;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * Created by juancarlos on 28/07/2017.
 */
public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "USERS.db";

    public DBHelper(Context context, String s, Object o, int i) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table usuarios(codigo integer primary key ,usuario text,contrasena text)");
        db.execSQL("create table estudiante(carnet integer primary key ,nombre text,placa1 text, placa2 text)");
        db.execSQL("insert into usuarios values(01,'admin@gmail.com','admin')");
        db.execSQL("insert into estudiante values(26926989,'guillermo','323cbl','412jkl')");

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("create table usuarios(codigo integer primary key ,usuario text,contrasena text)");
        db.execSQL("create table estudiante(carnet integer primary key ,nombre text,placa1 text, placa2 text)");
        db.execSQL("insert into usuarios values(01,'admin@gmail.com','admin')");
        db.execSQL("insert into estudiante values(26926989,'guillermo','323cbl','412jkl')");


    }
    public String[] buscar_reg(String buscar) {
        String[] datos = new String[5];
        SQLiteDatabase database = this.getWritableDatabase();
        String q = "Select * from estudiante where carnet = " + buscar;
        Cursor registros = database.rawQuery(q, null);
        if (registros.moveToFirst()) {
            for (int i = 0; i < 4; i++) {
                datos[i] = registros.getString(i);

            }
            datos[4]="Encontrado";


        }
        else {
            datos[4] = "no se encontro a " + buscar;

        }
        return datos;
    }
}