package com.example.juancarlos.appumg;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

/**
 * Created by juancarlos on 28/07/2017.
 */

public class Menu extends AppCompatActivity{
    EditText et1,et2,et3,et4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        et1= (EditText) findViewById(R.id.etcarnet);
        et2= (EditText) findViewById(R.id.etnombreusuario);
        et3= (EditText) findViewById(R.id.etplaca1);
        et4= (EditText) findViewById(R.id.etplaca2);

    }

    public void registrar(View view){

        DBHelper admin=new DBHelper(this,"USERS.db",null,1);
        SQLiteDatabase db=admin.getWritableDatabase();
        int carnetuser=Integer.parseInt(et1.getText().toString());
        String nombreusuario=et2.getText().toString();
        String placa1=et3.getText().toString();
        String placa2=et4.getText().toString();

        ContentValues values=new ContentValues();
        values.put("carnet",carnetuser);
        values.put("nombre",nombreusuario);
        values.put("placa1",placa1);
        values.put("placa2",placa2);

        db.insert("estudiante",null,values);
        db.close();

        Intent venmen=new Intent(this,Menu.class);
        startActivity(venmen);

    }
    public void consultas (View view){
        Intent vencon=new Intent(this,Consulta.class);
        startActivity(vencon);
    }
    public void cancelar(View view){
        Intent venlon=new Intent(this,Login.class);
        startActivity(venlon);

    }
}
