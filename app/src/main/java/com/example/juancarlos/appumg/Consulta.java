package com.example.juancarlos.appumg;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by juancarlos on 28/07/2017.
 */

public class Consulta extends AppCompatActivity{

    private EditText et1;
    private TextView carnet1, nombre1, placa11, placa21, color1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);

        et1 = (EditText) findViewById(R.id.etconsulcarnet);
        carnet1 = (TextView) findViewById(R.id.carnetc);
        nombre1 = (TextView) findViewById(R.id.nombrec);
        placa11 = (TextView) findViewById(R.id.placa1c);
        placa21 = (TextView) findViewById(R.id.placa2c);
        color1 = (TextView) findViewById(R.id.ResultadoDato);

    }


    public void consult (View v){
        DBHelper db = new DBHelper(getApplicationContext(),"USERS.db",null,1);
        String buscar = et1.getText().toString();
        String [] datos1;
        datos1 = db.buscar_reg(buscar);
        carnet1.setText(datos1[0]);
        nombre1.setText(datos1[1]);
        placa11.setText(datos1[2]);
        placa21.setText(datos1[3]);
        color1.setText(datos1[4]);

    }




}